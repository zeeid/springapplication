package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class GreetingController {
    @GetMapping("/greeting")
    String greeting(){return "Hello World";}

    @GetMapping("/name")
    String greeting(String name){return "Hello "+name;}
}
